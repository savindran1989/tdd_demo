/*
 * cli_os_mock.h
 *
 *  Created on: 14-Oct-2019
 *      Author: savindrak
 */

#ifndef UNIT_TESTS_MOCKS_CLI_OS_MOCK_H_
#define UNIT_TESTS_MOCKS_CLI_OS_MOCK_H_

void cli_os_mock_SetOperationType(uint8_t ucOperationType);
void cli_os_mock_SetNum1(int iNumber1);
void cli_os_mock_SetNum2(int iNumber2);

#endif /* UNIT_TESTS_MOCKS_CLI_OS_MOCK_H_ */
