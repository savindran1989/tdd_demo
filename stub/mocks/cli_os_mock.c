/*
 * cli_os_mock.c
 *
 *  Created on: 14-Oct-2019
 *      Author: savindrak
 */

#include <stdio.h>
#include <stdint.h>
#include "cli_os_mock.h"

static uint8_t m_ucOperationType;
static int     m_iNumber1;
static int     m_iNumber2;


uint8_t cli_os_GetOperationType(void)
{
    return m_ucOperationType;
}

int cli_os_GetNumber1(void)
{
    return m_iNumber1;
}

int cli_os_GetNumber2(void)
{
    return m_iNumber2;
}


void cli_os_mock_SetOperationType(uint8_t ucOperationType)
{
    m_ucOperationType = ucOperationType;
}

void cli_os_mock_SetNum1(int iNumber1)
{
    m_iNumber1 = iNumber1;
}

void cli_os_mock_SetNum2(int iNumber2)
{
    m_iNumber2 = iNumber2;
}

void cli_os_Exit(void)
{
  return;
}
