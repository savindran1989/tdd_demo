#include "CppUTest/TestHarness.h"
#include <string.h>
#include <stdio.h>
#include <stdint.h>

extern "C"
{
#include "cli.h"
#include "cli_os_mock.h"
}


TEST_GROUP(CliModule)
{
    void setup()
    {

    }

    void teardown()
    {

    }
};

TEST(CliModule, CliInit)
{
   cli_Init();
}

TEST(CliModule, GetUserInputAddition)
{
   tUserInput UserInput;        
   
   //setup test        
   cli_os_mock_SetOperationType(OP_ADDITION);
   cli_os_mock_SetNum1(20);
   cli_os_mock_SetNum2(40);
   //function under test	
   UserInput = cli_GetUserInput();
   //check function output
   CHECK_EQUAL(OP_ADDITION, UserInput.iOperationType);
   CHECK_EQUAL(20, UserInput.iNum1);
   CHECK_EQUAL(40, UserInput.iNum2);
}

TEST(CliModule, GetUserInputMultiplication)
{
   tUserInput UserInput;

   //setup test
   cli_os_mock_SetOperationType(OP_MULTIPLICATION);
   cli_os_mock_SetNum1(20);
   cli_os_mock_SetNum2(40);
   //function under test
   UserInput = cli_GetUserInput();
   //check function output
   CHECK_EQUAL(OP_MULTIPLICATION, UserInput.iOperationType);
   CHECK_EQUAL(20, UserInput.iNum1);
   CHECK_EQUAL(40, UserInput.iNum2);
}

TEST(CliModule, GetUserInputExitCalculator)
{
   tUserInput UserInput;

   //setup test
   cli_os_mock_SetOperationType(OP_EXIT);
   //function under test
   UserInput = cli_GetUserInput();
   //check function output
   CHECK_EQUAL(OP_EXIT, UserInput.iOperationType);
}

TEST(CliModule, ExecuteCalculatorAdditionOp)
{
    //setup test
    cli_os_mock_SetOperationType(OP_ADDITION);
    cli_os_mock_SetNum1(20);
    cli_os_mock_SetNum2(40);
    //function under test
    uint8_t ucResult = cli_ExecuteCalculator();
    //check function output
    CHECK_EQUAL(OP_SUCCEED, ucResult);
}

TEST(CliModule, ExecuteCalculatorMultiplicationOp)
{
    //setup test
    cli_os_mock_SetOperationType(OP_MULTIPLICATION);
    cli_os_mock_SetNum1(20);
    cli_os_mock_SetNum2(40);
    //function under test
    uint8_t ucResult = cli_ExecuteCalculator();
    //check function output
    CHECK_EQUAL(OP_SUCCEED, ucResult);
}

TEST(CliModule, ExecuteCalculatorWrongOp)
{
    //setup test
    cli_os_mock_SetOperationType(10);
    cli_os_mock_SetNum1(20);
    cli_os_mock_SetNum2(40);
    //function under test
    uint8_t ucResult = cli_ExecuteCalculator();
    //check function output
    CHECK_EQUAL(WRONG_INPUT, ucResult);
}

