#include "CppUTest/TestHarness.h"
#include <string.h>
#include <stdio.h>


extern "C"
{
#include "addition.h"
}

#define INVALID_SUM   3000

TEST_GROUP(AddModule)
{
    void setup()
    {

    }

    void teardown()
    {

    }
};

TEST(AddModule, InRangNum1Num2AddTest)
{ 
    //function under test
    int iSum = ad_Addition(10, 20);
    //check function output    
    CHECK_EQUAL(30, iSum);
}

TEST(AddModule, OutRangeNum1AddTest)
{
    //function under test 
    int iSum = ad_Addition(-1001, 20);
    //check function output   
    CHECK_EQUAL(INVALID_SUM, iSum);
}


TEST(AddModule, OutRangeNum2AddTest)
{
    //function under test 
    int iSum = ad_Addition(20, -1001);
    //check function output    
    CHECK_EQUAL(INVALID_SUM, iSum);
}

TEST(AddModule, OutRangeNum1Num2AddTest)
{
    //function under test 
    int iSum = ad_Addition(1005, -1001);
    //check function output    
    CHECK_EQUAL(INVALID_SUM, iSum);
}

TEST(AddModule, OutRangeNum1Num2AddTest1)
{
    //function under test 
    int iSum = ad_Addition(-5000, -1001);
    //check function output    
    CHECK_EQUAL(INVALID_SUM, iSum);
}

TEST(AddModule, OutRangeNum1Num2AddTest2)
{
    //function under test 
    int iSum = ad_Addition(5000, 1001);
    //check function output    
    CHECK_EQUAL(INVALID_SUM, iSum);
}
