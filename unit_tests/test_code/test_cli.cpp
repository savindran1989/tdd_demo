#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"
#include <string.h>
#include <stdio.h>
#include <stdint.h>

extern "C"
{
#include "cli.h"
}


TEST_GROUP(CliModule)
{
    void setup()
    {

    }

    void teardown()
    {
    	mock().clear();
    }
};

TEST(CliModule, CliInit)
{
   cli_Init();
}

TEST(CliModule, GetUserInputAddition)
{
   tUserInput UserInput = {0};
   int        iNumber1  = 10;
   int        iNumber2  = 20;
   
   //setup test
   mock().expectOneCall("cli_os_GetOperationType").andReturnValue(OP_ADDITION);
   mock().expectOneCall("cli_os_GetNumber1").andReturnValue(iNumber1);
   mock().expectOneCall("cli_os_GetNumber2").andReturnValue(iNumber2);
   //function under test	
   UserInput = cli_GetUserInput();
   //check expectations
   mock().checkExpectations();
   //check function output
   CHECK_EQUAL(OP_ADDITION, UserInput.iOperationType);
   CHECK_EQUAL(iNumber1, UserInput.iNum1);
   CHECK_EQUAL(iNumber2, UserInput.iNum2);
}


TEST(CliModule, GetUserInputMultiplication)
{
   tUserInput UserInput = {0};
   int        iNumber1  = 10;
   int        iNumber2  = 20;

   //setup test
   mock().expectOneCall("cli_os_GetOperationType").andReturnValue(OP_MULTIPLICATION);
   mock().expectOneCall("cli_os_GetNumber1").andReturnValue(iNumber1);
   mock().expectOneCall("cli_os_GetNumber2").andReturnValue(iNumber2);
   //function under test
   UserInput = cli_GetUserInput();
   //check expectations
   mock().checkExpectations();
   //check function output
   CHECK_EQUAL(OP_MULTIPLICATION, UserInput.iOperationType);
   CHECK_EQUAL(iNumber1, UserInput.iNum1);
   CHECK_EQUAL(iNumber2, UserInput.iNum2);
}

TEST(CliModule, GetUserInputExitCalculator)
{
   tUserInput UserInput;

   //setup test
   mock().expectOneCall("cli_os_GetOperationType").andReturnValue(OP_EXIT);
   mock().expectOneCall("cli_os_Exit");
   mock().expectOneCall("cli_os_GetNumber1").andReturnValue(10);
   mock().expectOneCall("cli_os_GetNumber2").andReturnValue(20);
   //function under test
   UserInput = cli_GetUserInput();
   //check expectations
   mock().checkExpectations();
   //check function output
   CHECK_EQUAL(OP_EXIT, UserInput.iOperationType);
}

TEST(CliModule, ExecuteCalculatorAdditionOp)
{
   //setup test
   int iNumber1 = 10;
   int iNumber2 = 20;

   //setup test
   mock().expectOneCall("cli_os_GetOperationType").andReturnValue(OP_ADDITION);
   mock().expectOneCall("cli_os_GetNumber1").andReturnValue(iNumber1);
   mock().expectOneCall("cli_os_GetNumber2").andReturnValue(iNumber2);
   //function under test
   uint8_t ucResult = cli_ExecuteCalculator();
   //check expectations
   mock().checkExpectations();
   //check function output
   CHECK_EQUAL(OP_SUCCEED, ucResult);
}

TEST(CliModule, ExecuteCalculatorMultiplicationOp)
{
   int iNumber1 = 10;
   int iNumber2 = 20;

   //setup test
   mock().expectOneCall("cli_os_GetOperationType").andReturnValue(OP_MULTIPLICATION);
   mock().expectOneCall("cli_os_GetNumber1").andReturnValue(iNumber1);
   mock().expectOneCall("cli_os_GetNumber2").andReturnValue(iNumber2);
   //function under test
   uint8_t ucResult = cli_ExecuteCalculator();
   //check expectations
   mock().checkExpectations();
   //check function output
   CHECK_EQUAL(OP_SUCCEED, ucResult);
}

TEST(CliModule, ExecuteCalculatorWrongOp)
{
    //setup test
    mock().expectOneCall("cli_os_GetOperationType").andReturnValue(50);
    mock().expectOneCall("cli_os_GetNumber1").andReturnValue(10);
    mock().expectOneCall("cli_os_GetNumber2").andReturnValue(20);
    //function under test
    uint8_t ucResult = cli_ExecuteCalculator();
    //check expectations
    mock().checkExpectations();
    //check function output
    CHECK_EQUAL(WRONG_INPUT, ucResult);
}

