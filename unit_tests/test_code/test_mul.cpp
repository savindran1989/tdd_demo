#include "CppUTest/TestHarness.h"
#include <string.h>
#include <stdio.h>


extern "C"
{
#include "multiplication.h"
}


TEST_GROUP(MulModule)
{
    void setup()
    {

    }

    void teardown()
    {

    }
};

TEST(MulModule, InRangNum1Num2MulTest)
{
   //function under test 
   int iResult = mul_Multiplication(10, 20);
   //check function output
   CHECK_EQUAL(200, iResult);
}

TEST(MulModule, InRangNum1Num2MulTest1)
{
   //function under test 
   int iResult = mul_Multiplication(10, -20);
   //check function output
   CHECK_EQUAL(-200, iResult);
}

TEST(MulModule, InRangNum1Num2MulTest2)
{
   //function under test 
   int iResult = mul_Multiplication(-10, -20);
   //check function output
   CHECK_EQUAL(200, iResult);
}

TEST(MulModule, OutRangNum1MulTest1)
{
   //function under test 
   int iResult = mul_Multiplication(300, 20);
   //check function output
   CHECK_EQUAL(INVALID_MUL, iResult);
}

TEST(MulModule, OutRangNum1MulTest2)
{
   //function under test 
   int iResult = mul_Multiplication(-500, 20);
   //check function output
   CHECK_EQUAL(INVALID_MUL, iResult);
}

TEST(MulModule, OutRangNum2MulTest1)
{
   //function under test 
   int iResult = mul_Multiplication(20, 500);
   //check function output
   CHECK_EQUAL(INVALID_MUL, iResult);
}

TEST(MulModule, OutRangNum2MulTest2)
{
   //function under test 
   int iResult = mul_Multiplication(20, -1000);
   //check function output
   CHECK_EQUAL(INVALID_MUL, iResult);
}


TEST(MulModule, OutRangNum1Num2MulTest1)
{
   //function under test 
   int iResult = mul_Multiplication(1000, 1000);
   //check function output
   CHECK_EQUAL(INVALID_MUL, iResult);
}


TEST(MulModule, OutRangNum1Num2MulTest2)
{
   //function under test 
   int iResult = mul_Multiplication(-201, -202);
   //check function output
   CHECK_EQUAL(INVALID_MUL, iResult);
}

