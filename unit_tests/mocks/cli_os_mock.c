/*
 * cli_os_mock.c
 *
 *  Created on: 14-Oct-2019
 *      Author: savindrak
 */
#include "CppUTestExt/MockSupport_c.h"
#include <stdio.h>
#include <stdint.h>


uint8_t cli_os_GetOperationType(void)
{
    mock_c()->actualCall("cli_os_GetOperationType");
    return mock_c()->intReturnValue();
}

int cli_os_GetNumber1(void)
{
    mock_c()->actualCall("cli_os_GetNumber1");
    return mock_c()->intReturnValue();
}

int cli_os_GetNumber2(void)
{
    mock_c()->actualCall("cli_os_GetNumber2");
    return mock_c()->intReturnValue();
}

void cli_os_Exit(void)
{
    mock_c()->actualCall("cli_os_Exit");
}

