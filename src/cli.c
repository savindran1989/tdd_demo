//! @addtogroup Cli
//! @{
//!
//****************************************************************************/
//! @file cli.c
//! @brief Give command line interface to user
//! @author Savindra Kumar(savindran1989@gmail.com)
//! @bug No known bugs.
//!
//****************************************************************************/
//                           Includes
//****************************************************************************/
//standard header files
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
//user defined files
#include "cli.h"
#include "cli_os.h"
#include "addition.h"
#include "multiplication.h"

//****************************************************************************/
//                           Defines and typedefs
//****************************************************************************/
#define PROJECT_TITLE     "Simple Calculator"
#define FIRMWARE_VERSION  "0.0.1"

//****************************************************************************/
//                           Global variables
//****************************************************************************/

//****************************************************************************/
//                           private variables
//****************************************************************************/

//****************************************************************************/
//                           Global Functions
//****************************************************************************/

//****************************************************************************/
//                           private Functions
//****************************************************************************/

//****************************************************************************/
//                    G L O B A L  F U N C T I O N S
//****************************************************************************/
void cli_Init(void)
{
    printf( "\r\n");
    printf("\tPROJECT.. [ %s ]\r\n", PROJECT_TITLE);
    printf("\tVERSION.. [ %s ]\r\n", FIRMWARE_VERSION);
    printf("\tDATE..... [ %s %s]\r\n", __DATE__, __TIME__);
    printf("\tPress....1 for Addition\r\n");
    printf("\tPress....2 for Multiplication\r\n");
    printf("\tPress....3 for exit\r\n");
}

tUserInput cli_GetUserInput(void)
{
    tUserInput UserInput = {0};

    printf("Type Operation: ");
    UserInput.iOperationType = cli_os_GetOperationType();

    if (OP_EXIT == UserInput.iOperationType)
    {
        printf("Exit from Calculator\r\n");
        cli_os_Exit();
    }

    printf("Enter Number1:  ");
    UserInput.iNum1 = cli_os_GetNumber1();
    printf("Enter Number2: ");
    UserInput.iNum2 = cli_os_GetNumber2();

    return UserInput;
}

uint8_t cli_ExecuteCalculator(void)
{
	uint8_t    ucResult        = OP_SUCCEED;
	tUserInput UserInput       = {0};
	int        iSum            = 0;
	int        iMultiplication = 0;

	UserInput = cli_GetUserInput();

	switch(UserInput.iOperationType)
	{
	case OP_ADDITION:
		iSum = ad_Addition(UserInput.iNum1, UserInput.iNum2);
		printf("Sum = %d\r\n", iSum);
		break;
	case OP_MULTIPLICATION:
		iMultiplication = mul_Multiplication(UserInput.iNum1, UserInput.iNum2);
		printf("Multiplication = %d\r\n", iMultiplication);
		break;
	default:
		ucResult = WRONG_INPUT;
		printf("Wrong User Input\r\n");
		break;
	}

	return ucResult;
}

//****************************************************************************/
//                           P R I V A T E  F U N C T I O N S
//****************************************************************************/

//****************************************************************************/
//                             End of file
//****************************************************************************/
/** @}*/
