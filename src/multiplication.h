//! @addtogroup Multiplication
//! @brief multiplication of two numbers
//! @{
//
//****************************************************************************
//! @file multiplication.h
//! @brief This contains the prototypes, macros, constants or global variables
//!        for multiplication
//! @author Savindra Kumar(savindran1989@gmail.com)
//! @bug No known bugs.
//
//****************************************************************************
#ifndef MUL_H
#define MUL_H

//****************************************************************************
//                           Includes
//****************************************************************************

//****************************************************************************
//                           Constants and typedefs
//****************************************************************************
#define INVALID_MUL   80000

//****************************************************************************
//                           Global variables
//****************************************************************************

//****************************************************************************
//                           Global Functions
//****************************************************************************
//
//! @brief Multiply two numbers
//! @param[in]  iNum1  Number1
//! @param[in]  iNum2  Number2
//! @param[out] None
//! @return     int32_t   Multiplication of Number1 and Number2
//
int mul_Multiplication(int iNum1, int iNum2);

#endif // MUL_H
//****************************************************************************
//                             End of file
//****************************************************************************
//! @}
