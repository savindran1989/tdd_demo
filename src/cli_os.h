//! @addtogroup CliOsLayer
//! @brief command line interface os layer
//! @{
//
//****************************************************************************
//! @file cli_os.h
//! @brief This contains the prototypes, macros, constants or global variables
//!        for Cli os layer functions
//! @author Savindra Kumar(savindran1989@gmail.com)
//! @bug No known bugs.
//
//****************************************************************************
#ifndef CLI_OS_H
#define CLI_OS_H

//****************************************************************************
//                           Includes
//****************************************************************************

//****************************************************************************
//                           Constants and typedefs
//****************************************************************************


//****************************************************************************
//                           Global variables
//****************************************************************************

//****************************************************************************
//                           Global Functions
//****************************************************************************
//
//! @brief  Get operation type
//! @param[in]  None
//! @param[out] None
//! @return     uint8_t Operation Type
//!             1 - Addition
//!             2 - Multiplication
//!             3 - Exit
//
uint8_t cli_os_GetOperationType(void);
//
//! @brief  Get Number1
//! @param[in]  None
//! @param[out] None
//! @return     int Number1
//
int cli_os_GetNumber1(void);
//
//! @brief  Get Number2
//! @param[in]  None
//! @param[out] None
//! @return     int Number2
//
int cli_os_GetNumber2(void);
//
//! @brief Exit from Calculator
//! @param[in]  None
//! @param[out] None
//! @return     None
//
void cli_os_Exit(void);

#endif // CLI_OS_H
//****************************************************************************
//                             End of file
//****************************************************************************
//! @}
