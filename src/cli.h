//! @addtogroup Cli
//! @brief command line interface for user
//! @{
//
//****************************************************************************
//! @file cli.h
//! @brief This contains the prototypes, macros, constants or global variables
//!        for Cli
//! @author Savindra Kumar(savindran1989@gmail.com)
//! @bug No known bugs.
//
//****************************************************************************
#ifndef CLI_H
#define CLI_H

//****************************************************************************
//                           Includes
//****************************************************************************

//****************************************************************************
//                           Constants and typedefs
//****************************************************************************
#define OP_ADDITION       1
#define OP_MULTIPLICATION 2
#define OP_EXIT           3
#define OP_SUCCEED        0
#define WRONG_INPUT       2


typedef struct UserInput
{
    int iOperationType;
    int iNum1;
    int iNum2;
}tUserInput;

//****************************************************************************
//                           Global variables
//****************************************************************************

//****************************************************************************
//                           Global Functions
//****************************************************************************
//
//! @brief  Display menu for user
//! @param[in]  None
//! @param[out] None
//! @return     None
//
void cli_Init(void);
//
//! @brief  Get User Input
//! @param[in]  None
//! @param[out] None
//! @return     tUserInput user input structure
//
tUserInput cli_GetUserInput(void);
//
//! @brief  Execute Calculator
//! @param[in]  None
//! @param[out] None
//! @return     uint8_t Calculator status
//
uint8_t cli_ExecuteCalculator(void);

#endif // CLI_H
//****************************************************************************
//                             End of file
//****************************************************************************
//! @}
